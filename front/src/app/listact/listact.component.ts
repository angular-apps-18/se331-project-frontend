import { User } from '../entity/user';
import { Activity } from '../entity/activity';
import { Teacher } from '../entity/teacher';
import { Enrol } from '../entity/enrol';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IdService } from '../services/id.service';
import { ActivityService } from '../services/activity.service';
import { TeacherService } from '../services/teacher.service';
import { EnrolService } from '../services/enrol.service';
import { MatTableDataSource, MatSort, MatInput } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

class Row {
    id: number;
    name: string;
    loc: string;
    desc: string;
    regStart: string;
    regEnd: string;
    when: string;
    time: string;
    teacher: string;
    disabled: boolean;
}

@Component({
    selector: 'app-listact',
    templateUrl: './listact.component.html',
    styleUrls: ['./listact.component.css']
})
export class ListactComponent implements OnInit {
    id: User;
    activities: Activity[];
    teachers: Teacher[];
    enrols: Enrol[];
    comments: string[];
    actTable = new MatTableDataSource<Row>();
    @ViewChild(MatSort) sort: MatSort;
    columns: string[] = [
        'id', 'name', 'loc', 'desc', 'when', 'time',
        'teacher', 'regStart', 'regEnd', 'comment', 'enrol'
    ];
    filterForm: FormGroup;
    minDate: string;
    maxDate: string;

    constructor(private idSrv: IdService,
                private activitySrv: ActivityService,
                private teacherSrv: TeacherService,
                private enrolSrv: EnrolService,
                private fb: FormBuilder) {
        this.filterForm = this.fb.group({
            filter:  null,
            minDate: null,
            maxDate: null,
        });
        this.idSrv.obsUser().subscribe(id => {
            this.id = id;
            this.makeRows();
        });
        this.activitySrv.obs().subscribe(activities => {
            this.activities = activities;
            this.makeRows();
        });
        this.teacherSrv.obs().subscribe(teachers => {
            this.teachers = teachers;
            this.makeRows();
        });
        this.enrolSrv.obs().subscribe(enrols => {
            this.enrols = enrols;
            this.makeRows();
        });
    }

    makeRows() {
        if (!(this.id && this.activities && this.teachers && this.enrols)) return;
        let sid: number = this.id.id;
        let today: string = new Date().toISOString().substring(0,10);
        let data: Row[] = new Array<Row>();
        for (let act of this.activities) {
            let enrol = this.enrolSrv.getPair(sid, act.id);
            let disabled = enrol != null;
            disabled = disabled || act.regStart > today;
            disabled = disabled || act.regEnd < today;
            let t = this.teacherSrv.get(act.teacher);
            let row: Row = {
                id: act.id,
                name: act.name,
                loc: act.loc,
                desc: act.desc,
                regStart: act.regStart,
                regEnd: act.regEnd,
                when: act.when,
                time: act.time,
                teacher: t.name,
                disabled: disabled
            }
            data.push(row);
        }
        this.actTable.data = data;
    }

    filterPred = (row: Row, filter: string) => {
        let accept: boolean = row.name.toLowerCase().indexOf(filter.substring(1)) > -1;
        if (this.minDate) accept = accept && row.when >= this.minDate;
        if (this.maxDate) accept = accept && row.when <= this.maxDate;
        return accept;
    }

    updateFilter() {
        let fmVal = this.filterForm.value;
        let filter = fmVal.filter == null ? "" :
                     fmVal.filter.trim().toLowerCase();
        this.minDate = fmVal.minDate;
        this.maxDate = fmVal.maxDate;
        this.actTable.filter = "." + filter;
        this.makeRows();
    }

    ngOnInit() {
        this.actTable.sort = this.sort;
        this.actTable.filterPredicate = this.filterPred;
        this.actTable.filter = ".";
    }

    enrol(actid: number) {
        let enrol: Enrol = {
            id: 0,
            student: this.id.id,
            activity: actid,
            status: "pending"
        }
        this.enrolSrv.add(enrol);
    }
    
    addComment(cm: string){
        this.comments.push(cm);
        console.log(this.comments);
    }

}
