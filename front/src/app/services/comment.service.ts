import { Observable } from 'rxjs';
import { Comment } from '../entity/comment';

export abstract class CommentService {
    abstract load();
    abstract obs(): Observable<Comment[]>;
    abstract add(comment: Comment): Comment;
    abstract get(id: number): Comment;
    abstract del(id: number): Comment;
}
