import { Teacher } from '../entity/teacher';
import { TeacherService } from './teacher.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeacherFileImplService extends TeacherService {
    private teachers = new BehaviorSubject<Teacher[]>(null);

    constructor(private http: HttpClient) {
        super();
        this.http.get<Teacher[]>('assets/teachers.json').subscribe(teachers => {
            this.teachers.next(teachers);
        });
    }

    load() { }

    obs(): Observable<Teacher[]> {
        return this.teachers.asObservable();
    }

    get(id: number): Teacher {
        if (this.teachers == null) return null;
        let list = this.teachers.getValue();
        return list.find((obj) => obj.id === id);
    }

    private nextId(): number {
        let list = this.teachers.getValue();
        return list.map(obj => obj.id).reduce((a,b) => a > b ? a : b) + 1;
    }

    add(teacher: Teacher): Teacher {
        if (this.teachers == null) return null;
        let list = this.teachers.getValue();
        list.push(teacher);
        this.teachers.next(list);
        return teacher;
    }

    del(id: number): Teacher {
        if (this.teachers == null) return null;
        let list = this.teachers.getValue();
        let teacher = list.find((obj) => obj.id === id);
        if (teacher === null) return null;
        let i = list.indexOf(teacher);
        list.splice(i, 1);
        this.teachers.next(list);
        return teacher;
    }

    upd(teacher: Teacher): Teacher {
        if (this.teachers == null) return null;
        let list = this.teachers.getValue();
        let old = list.find((obj) => obj.id === teacher.id);
        if (old === null) return null;
        let i = list.indexOf(old);
        list[i] = teacher;
        this.teachers.next(list);
        return old;
    }

}

