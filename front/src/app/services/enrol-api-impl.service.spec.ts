import { TestBed, inject } from '@angular/core/testing';

import { EnrolApiImplService } from './enrol-api-impl.service';

describe('EnrolApiImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnrolApiImplService]
    });
  });

  it('should be created', inject([EnrolApiImplService], (service: EnrolApiImplService) => {
    expect(service).toBeTruthy();
  }));
});
