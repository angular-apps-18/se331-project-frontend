import { Component, OnInit } from '@angular/core';
import { User } from '../entity/user';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PassErrorStateMatcher } from '../core/errorstatematcher';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { IdService } from '../services/id.service';
import { environment } from 'src/environments/environment';
import { TeacherService } from '../services/teacher.service';
import { Teacher } from '../entity/teacher';
import { TeacherUser } from '../entity/teacheruser';

@Component({
    selector: 'app-tprofile',
    templateUrl: './tprofile.component.html',
    styleUrls: ['./tprofile.component.css']
})
export class TprofileComponent implements OnInit {
    private api = environment.tprofileApi;
    user: User;
    teacher: Teacher = {
        id: 0,
        name: "",
        surname: "",
        imgurl: "",
    };
    teachers: Teacher[];
    pfForm: FormGroup;
    fb: FormBuilder = new FormBuilder();
    matcher = new PassErrorStateMatcher();
    confirmPass = "";

    constructor(private router: Router,
        private teacherSrv: TeacherService,
        private http: HttpClient,
        private idSrv: IdService) {
        this.pfForm = this.fb.group({
            name: [null, Validators.required],
            surname: [null, Validators.required],
            imgurl: [null, Validators.required],
            password: [null, Validators.required],
            confirmPassword: [''],
            email: [null, Validators.compose([Validators.required, Validators.email])],
        }, { validator: this.checkPasswords });
        this.teacherSrv.obs().subscribe(teachers => {
            this.teachers = teachers;
            this.getTeacherProfile();
        });

        this.idSrv.obsUser().subscribe(user => {
            this.user = user;
            this.confirmPass = this.user.pass;
            this.getTeacherProfile();
        });

    }

    getTeacherProfile() {
        if (this.teachers == null || this.user == null) {
            return;
        }
        let fm = this.pfForm;
        for (let t of this.teachers) {
            if (t.id == this.user.id) {
                this.teacher = t;
                fm.get('name').setValue(t.name);
                fm.get('surname').setValue(t.surname);
                fm.get('imgurl').setValue(t.imgurl);
                fm.get('email').setValue(this.user.email);
                fm.get('password').setValue(this.user.pass);
                fm.get('confirmPassword').setValue(this.user.pass);
            }
        }
    }

    checkPasswords(group: FormGroup) {
        let pass = group.controls.password.value;
        let confirmPass = group.controls.confirmPassword.value;
        return pass === confirmPass ? null : { notSame: true }
    }


    ngOnInit() { }

    validation_messages = {
        'name': [
            { type: 'required', message: 'the name is required' }
        ],
        'surname': [
            { type: 'required', message: 'the surname is required' }
        ],
        'imgurl': [
            { type: 'required', message: 'the image link is required' }
        ],
        'password': [
            { type: 'minLength', message: 'the minmium length is 6' },
            { type: 'required', message: 'the password is required' },
        ],
        'email': [
            { type: 'required', message: 'email is required' },
            { type: 'email', message: 'email format is not correct' }
        ],
        'confirmPassword': [
            { type: 'required', message: 'confirm password is required' },
        ]
    };

    update() {
        let fmVal = this.pfForm.value;
        let newTeUsr: TeacherUser = {
            id: 0,
            name: fmVal.name,
            email: fmVal.email,
            pass: fmVal.password,
            surname: fmVal.surname,
            imgurl: fmVal.imgurl
        }
        this.http.put<any>(this.api + "/" + this.user.id, newTeUsr)
            .subscribe(data => {
                this.teacherSrv.load();
                alert('Update successfully');
            });
        this.router.navigate(['/tprofile'])
    }
}
