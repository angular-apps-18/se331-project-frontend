import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IdService } from '../services/id.service';
import { NavComponent } from '../nav/nav.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { User } from '../entity/user';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    fb: FormBuilder = new FormBuilder();
    loginForm: FormGroup;
    loginSubs: Subscription;
    wrongCreds = false;
    wrongTimer = null;

    constructor(private router: Router,
        private idService: IdService,
        private nav: NavComponent) { }

    ngOnInit(): void { 
        this.wrongCreds = false;
        this.loginForm = this.fb.group({
            username: [null,[Validators.required]],
            password: ['',  [Validators.required]],
        })
    }

    validation_messages = {
        'password': [ { type: 'required', message: 'password is required' } ],
        'username': [ { type: 'required', message: 'username is required' } ]
    }

    myEvent(event) {
        if (event.keyCode === 13) this.login();
    }

    login() {
        this.wrongCreds = false;
        let username = this.loginForm.value.username;
        let password = this.loginForm.value.password;
        this.loginSubs = this.idService.login(username, password)
            .subscribe((user) => { this.afterLogin(user); });
    }

    afterLogin(user: User) {
        if (user == null) {
            this.wrongTimer = setTimeout(
                () => this.wrongCreds = true, 1000);
        } else {
            clearTimeout(this.wrongTimer);
            this.wrongCreds = false;
            this.nav.setUser(user);
            if (user.type == 'new-student') {
                this.router.navigate(["/wait"]);
            } else if (user.type == 'student') {
                this.router.navigate(["/listact"]);
            } else if (user.type == 'teacher') {
                this.router.navigate(["/tlistact"]);
            } else if (user.type == 'admin') {
                this.router.navigate(["/chkstudents"]);
            }
            setTimeout(() => { this.loginSubs.unsubscribe(); }, 500);
        }
    }
}
