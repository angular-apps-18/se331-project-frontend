import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelCommentsComponent } from './del-comments.component';

describe('DelCommentsComponent', () => {
  let component: DelCommentsComponent;
  let fixture: ComponentFixture<DelCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
