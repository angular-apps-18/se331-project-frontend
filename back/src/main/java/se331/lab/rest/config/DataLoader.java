package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.entity.*;
import se331.lab.rest.repository.*;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Component
public class DataLoader implements ApplicationRunner {
  @Autowired
  StudentRepository studentRepository;
  @Autowired
  TeacherRepository teacherRepository;
  @Autowired
  ActivityRepository activityRepository;
  @Autowired
  AuthorityRepository authorityRepository;
  @Autowired
  UserRepository userRepository;
  @Autowired
  EnrolsRepository enrolsRepository;
  @Autowired
  AdminRepository adminRepository;
  @Autowired
  CommentRepository commentRepository;

  @Override
  @Transactional
  public void run(ApplicationArguments args) throws Exception {
    Student student1 = Student.builder()
      .stid("592115502")
      .name("Guoqing")
      .surname("Li")
      .imgurl("https://s3-ap-southeast-1.amazonaws.com/se331/pics/tu.jpg")
      .dob("1992-03-09")
      .build();
    Student student2 = Student.builder()
      .stid("592115515")
      .name("Mark")
      .surname("Xiao")
      .imgurl("https://drive.google.com/uc?id=1k3EeC4ywp_pdHgwp_-J458kidHRcxzJz")
      .dob("1998-07-16")
      .build();
    Student student3 = Student.builder()
      .stid("592115000")
      .name("James")
      .surname("Blank")
      .imgurl(null)
      .dob("2001-01-01")
      .build();
    Student student4 = Student.builder()
      .stid("592115514")
      .name("Tach")
      .surname("Nazarov")
      .imgurl("https://drive.google.com/uc?id=1ilgBKln1Isgw9JcBz5yqBoB8QZQROvQs")
      .dob("1999-04-23")
      .build();
    Student student5 = Student.builder()
      .stid("592115516")
      .name("Nile")
      .surname("Lin")
      .imgurl("https://drive.google.com/uc?id=1XtJKUvQQFU8JWV0walpNsdzWBAlcvqeE")
      .dob("1999-04-23")
      .build();
    Student student6 = Student.builder()
      .stid("592115029")
      .name("Saksit")
      .surname("Namhung")
      .imgurl("https://drive.google.com/uc?id=1lLY1M-2B3WCdqCRT443LIJxMkmbSGxNA")
      .dob("1997-12-26")
      .build();

    this.studentRepository.save(student1);
    this.studentRepository.save(student2);
    this.studentRepository.save(student3);
    this.studentRepository.save(student4);
    this.studentRepository.save(student5);
    this.studentRepository.save(student6);

    Teacher teacher1 = Teacher.builder()
      .name("Chartchai")
      .surname("Doungsa-ard")
      .imgurl("https://drive.google.com/uc?id=1dPOPia3Fhneb5FUHdHIANMgyi8vdf1Ac")
      .build();
    Teacher teacher2 = Teacher.builder()
      .name("Pree")
      .surname("Thiengburanathum")
      .imgurl("https://drive.google.com/uc?id=1n3ZkZjVaCUWKW9LiepMtxQCWPJmohgqA")
      .build();
    Teacher teacher3 = Teacher.builder()
      .name("Jayakrit")
      .surname("Hirisajja")
      .imgurl("https://drive.google.com/uc?id=178t3U7RwRQnU5jRvx_XTj2DzToiSQREw")
      .build();
    Teacher teacher4 = Teacher.builder()
      .name("Teuy")
      .surname("Aj.")
      .imgurl("https://drive.google.com/uc?id=1KX49TNuHsvKXjyFtzbsLflz0OdyopuyU")
      .build();
    this.teacherRepository.save(teacher1);
    this.teacherRepository.save(teacher2);
    this.teacherRepository.save(teacher3);
    this.teacherRepository.save(teacher4);
//    List<String> comment1 = new ArrayList<>();
//    List<String> comment2 = new ArrayList<>();
//    List<String> comment3 = new ArrayList<>();
//    List<String> comment4 = new ArrayList<>();
//    List<String> comment5 = new ArrayList<>();
    Comment c1 = Comment.builder()
      .msg("This is good!!!")
      .time("08/02/2018")
      .activity(1)
      .avatar(teacher1.getImgurl())
      .imgurl("https://drive.google.com/uc?id=1UskRQ0sjYAJeXJ0Sk-xSljv_wyoEeIdm")
      .name("Chartchai")
      .build();
    Comment c2 = Comment.builder()
      .msg("This is very good!!!")
      .time("09/02/2018")
      .activity(1)
      .avatar(student2.getImgurl())
      .imgurl("https://drive.google.com/uc?id=1jAF1EpnbNthqiTPziufGFuUumjfol7_9")
      .name("Mark")
      .build();
    Comment c3 = Comment.builder()
      .msg("This is bad!!!")
      .time("19/02/2018")
      .activity(2)
      .avatar(student1.getImgurl())
      .imgurl("https://drive.google.com/uc?id=12vg5DZWTZ5YDvck4Uyy0WwSJ1y5v6MiY")
      .name("Guoqing")
      .build();
    Comment c4 = Comment.builder()
      .msg("Maybe it is bad!!!")
      .time("18/02/2018")
      .activity(2)
      .avatar(teacher3.getImgurl())
      .imgurl("https://drive.google.com/uc?id=1Klb_6KUqC37rcpYiDk5JA-XMuR3IejIz")
      .name("Pree")
      .build();
    Comment c5 = Comment.builder()
      .msg("No one join it!!!")
      .time("15/02/2018")
      .activity(3)
      .avatar(teacher3.getImgurl())
      .imgurl(null)
      .name("Pree")
      .build();
    Comment c6 = Comment.builder()
      .msg("Waste time!!!")
      .time("22/02/2018")
      .activity(4)
      .avatar(student3.getImgurl())
      .imgurl(null)
      .name("James")
      .build();
    Comment c7 = Comment.builder()
      .msg("It is true!!!")
      .time("23/02/2018")
      .activity(4)
      .avatar(teacher4.getImgurl())
      .imgurl(null)
      .name("Teuy")
      .build();
    Comment c8 = Comment.builder()
      .msg("No one join it????")
      .time("20/02/2018")
      .activity(5)
      .avatar(teacher3.getImgurl())
      .imgurl(null)
      .name("Jayakrit")
      .build();
    this.commentRepository.save(c1);
    this.commentRepository.save(c2);
    this.commentRepository.save(c3);
    this.commentRepository.save(c4);
    this.commentRepository.save(c5);
    this.commentRepository.save(c6);
    this.commentRepository.save(c7);
    this.commentRepository.save(c8);

    Activity activity1 = Activity.builder()
      .desc("We will go to see the hippo and the panda")
      .loc("Chiang Mai Zoo")
      .name("Visit the zoo")
      .when("2019-10-31")
      .time("12:00")
      .regStart("2018-10-14")
      .regEnd("2019-10-29")
      .build();
    Activity activity2 = Activity.builder()
      .desc("Learn about the ancient murals")
      .loc("Wat Umong")
      .name("Explore Wat Umong")
      .when("2019-11-30")
      .time("12:00")
      .regStart("2018-11-14")
      .regEnd("2019-11-29")
      .build();
    Activity activity3 = Activity.builder()
      .desc("Learn to scuba dive in Koh Samui")
      .loc("Koh Samui")
      .name("Introduction to Scuba")
      .when("2019-12-31")
      .time("12:00")
      .regStart("2018-12-14")
      .regEnd("2019-12-29")
      .build();
    Activity activity4 = Activity.builder()
      .desc("We will learn how to sail in small two-person dinghy boats")
      .loc("Huay Tung Tao lake")
      .name("Learn to sail")
      .when("2019-10-27")
      .time("09:00")
      .regStart("2018-08-10")
      .regEnd("2019-10-27")
      .build();
    Activity activity5 = Activity.builder()
      .desc("All day trek up to the top of Doi Inthanon mountain")
      .loc("Doi Inthanon")
      .name("Mountain trekking")
      .when("2019-11-03")
      .time("06:00")
      .regStart("2018-08-26")
      .regEnd("2019-11-02")
      .build();
    this.activityRepository.save(activity1);
    this.activityRepository.save(activity2);
    this.activityRepository.save(activity3);
    this.activityRepository.save(activity4);
    this.activityRepository.save(activity5);
    Enrols enrols1 = Enrols.builder()
      .activity(2)
      .student(1)
      .status("confirmed")
      .build();
    Enrols enrols2 = Enrols.builder()
      .activity(1)
      .student(2)
      .status("confirmed")
      .build();
    Enrols enrols3 = Enrols.builder()
      .activity(3)
      .student(1)
      .status("rejected")
      .build();
    Enrols enrols4 = Enrols.builder()
      .activity(3)
      .student(3)
      .status("pending")
      .build();
    Enrols enrols5 = Enrols.builder()
      .activity(4)
      .student(3)
      .status("confirmed")
      .build();
    this.enrolsRepository.save(enrols1);
    this.enrolsRepository.save(enrols2);
    this.enrolsRepository.save(enrols3);
    this.enrolsRepository.save(enrols4);
    this.enrolsRepository.save(enrols5);
    activity1.setLecturer(teacher1);
    teacher1.getActivities().add(activity1);
    activity2.setLecturer(teacher2);
    teacher2.getActivities().add(activity2);
    activity3.setLecturer(teacher2);
    teacher2.getActivities().add(activity3);
    activity4.setLecturer(teacher4);
    teacher4.getActivities().add(activity4);
    activity5.setLecturer(teacher3);
    teacher3.getActivities().add(activity5);

    Admin admin = Admin.builder()
      .name("admin")
      .surname("admin")
      .imgurl("https://drive.google.com/uc?id=1W9VLWi53eG8y6pL25Yo68A6ig9C5F32a")
      .build();
    adminRepository.save(admin);
    PasswordEncoder encoder = new BCryptPasswordEncoder();
    Authority auth1 = Authority.builder().name(AuthorityName.ROLE_ADMIN).build();
    Authority auth2 = Authority.builder().name(AuthorityName.ROLE_TEACHER).build();
    Authority auth3 = Authority.builder().name(AuthorityName.ROLE_STUDENT).build();
    User user1, user2, user3, user4, user5, user6, user7, user8, user9, user10, user11;
    user1 = User.builder()
      .pass(encoder.encode("admin"))
      .email("admin@test.com")
      .enabled(true)
      .status("ok")
      .type("admin")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user2 = User.builder()
      .pass(encoder.encode("dto"))
      .email("dto@test.com")
      .enabled(true)
      .status("ok")
      .type("teacher")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user3 = User.builder()
      .pass(encoder.encode("pree"))
      .email("pree@test.com")
      .enabled(true)
      .status("ok")
      .type("teacher")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user4 = User.builder()
      .pass(encoder.encode("joe"))
      .email("joe@test.com")
      .enabled(true)
      .status("ok")
      .type("teacher")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user5 = User.builder()
      .pass(encoder.encode("teuy"))
      .email("teuy@test.com")
      .enabled(true)
      .status("ok")
      .type("teacher")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user6 = User.builder()
      .pass(encoder.encode("lee"))
      .email("lee@test.com")
      .enabled(true)
      .status("ok")
      .type("student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user7 = User.builder()
      .pass(encoder.encode("mark"))
      .email("mark@test.com")
      .enabled(true)
      .status("ok")
      .type("student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user8 = User.builder()
      .pass(encoder.encode("james"))
      .email("james@test.com")
      .enabled(true)
      .status("ok")
      .type("student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user9 = User.builder()
      .pass(encoder.encode("tach"))
      .email("tach@test.com")
      .enabled(true)
      .status("new")
      .type("new-student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user10 = User.builder()
      .pass(encoder.encode("nile"))
      .email("nile@test.com")
      .enabled(false)
      .status("reject")
      .type("new-student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user11 = User.builder()
      .pass(encoder.encode("saksit"))
      .email("saksit@test.com")
      .enabled(false)
      .status("reject")
      .type("new-student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();

    authorityRepository.save(auth1);
    authorityRepository.save(auth2);
    authorityRepository.save(auth3);
    user1.getAuthorities().add(auth1);
    user1.getAuthorities().add(auth2);
    user1.getAuthorities().add(auth3);

    user2.getAuthorities().add(auth2);
    user2.getAuthorities().add(auth3);

    user3.getAuthorities().add(auth2);
    user3.getAuthorities().add(auth3);

    user4.getAuthorities().add(auth2);
    user4.getAuthorities().add(auth3);

    user5.getAuthorities().add(auth2);
    user5.getAuthorities().add(auth3);

    user6.getAuthorities().add(auth3);

    user7.getAuthorities().add(auth3);

    user8.getAuthorities().add(auth3);

    user9.getAuthorities().add(auth3);

    user10.getAuthorities().add(auth3);

    user11.getAuthorities().add(auth3);
    userRepository.save(user6);
    userRepository.save(user7);
    userRepository.save(user8);
    userRepository.save(user9);
    userRepository.save(user10);
    userRepository.save(user11);
    userRepository.save(user2);
    userRepository.save(user3);
    userRepository.save(user4);
    userRepository.save(user5);
    userRepository.save(user1);


    admin.setUser(user1);
    user1.setAppUser(admin);

    teacher1.setUser(user2);
    user2.setAppUser(teacher1);

    teacher2.setUser(user3);
    user3.setAppUser(teacher2);

    teacher3.setUser(user4);
    user4.setAppUser(teacher3);

    teacher4.setUser(user5);
    user5.setAppUser(teacher4);

    student1.setUser(user6);
    user6.setAppUser(student1);

    student2.setUser(user7);
    user7.setAppUser(student2);

    student3.setUser(user8);
    user8.setAppUser(student3);

    student4.setUser(user9);
    user9.setAppUser(student4);

    student5.setUser(user10);
    user10.setAppUser(student5);

    student6.setUser(user11);
    user11.setAppUser(student6);

  }


}
