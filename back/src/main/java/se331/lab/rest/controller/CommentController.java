package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.lab.rest.entity.dto.CommentDTO;
import se331.lab.rest.service.CommentService;

import java.util.stream.Collectors;

@Controller
public class CommentController {
  @Autowired
  CommentService commentService;
  @CrossOrigin
  @GetMapping("/comments")
  public ResponseEntity<?> getComments(){
    return ResponseEntity.ok(commentService.getComment().stream()
      .map(CommentDTO::getCommentDTO).collect(Collectors.toList()));
  }
  @CrossOrigin
  @PostMapping("/comments")
  public ResponseEntity<?> saveComment(@RequestBody CommentDTO commentDTO) {
    return ResponseEntity.ok(commentService.save(commentDTO.getComment()));
  }
  @CrossOrigin
  @DeleteMapping("/comments/{id}")
  public ResponseEntity<?> deleteComment(@PathVariable long id) {
    return ResponseEntity.ok(commentService.deleteById(id));
  }
  @CrossOrigin
  @PutMapping("/comments/{id}")
  public ResponseEntity<?> saveCommentByPut(@PathVariable long id,@RequestBody CommentDTO commentDTO) {
    commentDTO.setId(id);
    return ResponseEntity.ok(commentService.save(commentDTO.getComment()));
  }
}
