package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.dao.EnrolsDao;
import se331.lab.rest.entity.Enrols;

import java.util.List;

@Service
public class EnrolsServiceImpl implements EnrolsService{
  private final
  EnrolsDao enrolsDao;
  @Autowired
  public EnrolsServiceImpl(EnrolsDao enrolsDao) {
    this.enrolsDao = enrolsDao;
  }

  @Transactional
  @Override
  public List<Enrols> getEnrols() {
    return enrolsDao.findAll();
  }

  @Override
  @Transactional
  public Enrols save(Enrols enrols) {
    return enrolsDao.save(enrols);
  }

  @Override
  public Enrols deleteById(long id) {
    return enrolsDao.deleteById(id);
  }
}
