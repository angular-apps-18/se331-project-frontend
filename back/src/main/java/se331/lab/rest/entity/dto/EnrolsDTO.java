package se331.lab.rest.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.entity.Enrols;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnrolsDTO {
  Long id;
  int student;
  int activity;
  String status;


  public static EnrolsDTO getEnrolsListDTO(Enrols enrols) {
    return EnrolsDTO.builder()
      .id(enrols.getId())
      .student(enrols.getStudent())
      .activity(enrols.getActivity())
      .status(enrols.getStatus())
      .build();
    
  }

  public Enrols getEnrols() {
    return Enrols.builder()
      .id(this.id)
      .activity(this.activity)
      .student(this.student)
      .status(this.status)
      .build();
  }
}
